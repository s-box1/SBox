import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:s_box/utils/app_color.dart';
import 'package:s_box/utils/app_style.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: AppColor.primary,
          statusBarIconBrightness: Brightness.light, // For Android (dark icons)
          statusBarBrightness: Brightness.light, // For iOS (dark icons)
        ),
        backgroundColor: AppColor.primary,
        centerTitle: true,
        title: const Text(
          "S-Box",
          style: AppStyle.title,
        ),
      ),
      body: Center(
        child: Text("Home"),
      ),
    );
  }
}
