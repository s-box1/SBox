import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rxdart/rxdart.dart';
import 'package:s_box/data/shared_preferences/shared_preferences.dart';
import 'package:s_box/module/home/home_page.dart';
import 'package:s_box/module/login/login_page.dart';
import 'package:s_box/utils/app_color.dart';
import 'package:s_box/utils/common_method.dart';
import 'package:s_box/utils/constant.dart';

String? cookie;

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  PublishSubject loadConfigurePublishSubject = PublishSubject<dynamic>();

  @override
  void initState() {
    super.initState();
    _startApp();
    checkPermission();
    _listenConfigureApp();
  }

  @override
  void dispose() {
    super.dispose();
    loadConfigurePublishSubject.close();
  }

  Future<void> _startApp() async {
    // await _loadThemeApp();
    // await _loadLanguageApp();
    await _checkLogin();
  }

  Future<void> _checkLogin() async {
    cookie = await SPref.instance.get(SPrefCache.KEY_ACCESS_TOKEN);

    // print Token in console
    printWrappedLog('Token: $cookie');
    loadConfigurePublishSubject.add(cookie);
  }

  void _listenConfigureApp() {
    // Delay 1s
    loadConfigurePublishSubject.stream
        .delay(const Duration(seconds: 1))
        .listen((value) {
      if (value != null && value.toString().isNotEmpty) {
        print('Navigate Splash Screen to Home Page');
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (_) => HomePage()));
        return;
      }
      print('Navigate to Login Page');
      // Navigator.pushReplacement(
      //     context, MaterialPageRoute(builder: (_) => LoginPage()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            color: AppColor.primaryBackgroundColor,
            // image:  const DecorationImage(
            //   fit: BoxFit.cover,
            //   image: ExactAssetImage('assets/images/logo_ptit.jpg'),
            // ),
          ),
          child: Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Spacer(),
                    SvgPicture.asset(
                      "assets/images/ptit.svg",
                      width: 200,
                      height: 200,
                      fit: BoxFit.scaleDown,
                    ),
                    const Spacer(),
                    const CircularProgressIndicator(
                      backgroundColor: Colors.white,
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
                      semanticsLabel: 'Loading',
                    ),
                    const Spacer(),
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Học Viện Công Nghệ BC - VT\ncơ sở TP. HCM ',
                            style: TextStyle(
                              color: AppColor.primaryHintColor,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
