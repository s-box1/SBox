import 'package:flutter/material.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:s_box/module/home/home_page.dart';
import 'package:s_box/module/notification/notification_page.dart';
import 'package:s_box/module/profile/profile_page.dart';
import 'package:s_box/module/study_space/study_space_page.dart';
import 'package:s_box/module/utilities/utilities_page.dart';
import 'package:s_box/utils/app_color.dart';
import 'package:s_box/utils/app_style.dart';

class MyNavPage extends StatefulWidget {
  const MyNavPage({Key? key}) : super(key: key);

  @override
  _MyNavPageState createState() => _MyNavPageState();
}

class _MyNavPageState extends State<MyNavPage> {
  int _currentIndex = 0;
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox.expand(
        child: PageView(
          controller: _pageController,
          onPageChanged: (index) {
            setState(() => _currentIndex = index);
          },
          children:  [
            HomePage(),
            NotificationPage(),
            StudySpacePage(),
            UtilitiesPage(),
            ProfilePage(),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        onItemSelected: (index) {
          setState(() => _currentIndex = index);
          _pageController.jumpToPage(index);
        },
        items: <BottomNavyBarItem>[
          BottomNavyBarItem(
              activeColor: AppColor.primary,
              inactiveColor: AppColor.grey,
              textAlign: TextAlign.center,
              title: const Text(
                'Trang chủ',
                style: AppStyle.body2,
              ),
              icon: const Icon(Icons.home_filled)),
          BottomNavyBarItem(
              activeColor: AppColor.primary,
              inactiveColor: AppColor.grey,
              textAlign: TextAlign.center,
              title: const Text(
                'Thông báo',
                style: AppStyle.body2,
              ),
              icon: const Icon(Icons.notifications_rounded)),
          BottomNavyBarItem(
              activeColor: AppColor.primary,
              inactiveColor: AppColor.grey,
              textAlign: TextAlign.center,
              title: const Text(
                'Góc học tập',
                style: AppStyle.body2,
              ),
              icon: const Icon(Icons.art_track_rounded)),
          BottomNavyBarItem(
              activeColor: AppColor.primary,
              inactiveColor: AppColor.grey,
              textAlign: TextAlign.center,
              title: const Text(
                'Tiện ích',
                style: AppStyle.body2,
              ),
              icon: const Icon(Icons.all_out_outlined)),
          BottomNavyBarItem(
              activeColor: AppColor.primary,
              inactiveColor: AppColor.grey,
              textAlign: TextAlign.center,
              title: const Text(
                'Tài khoản',
                style: AppStyle.body2,
              ),
              icon: const Icon(Icons.account_circle)),
        ],
      ),
    );
  }
}
