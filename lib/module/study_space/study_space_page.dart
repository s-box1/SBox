import 'package:flutter/material.dart';

class StudySpacePage extends StatelessWidget {
  const StudySpacePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text("StudySpacePage"),
      ),
    );
  }
}
