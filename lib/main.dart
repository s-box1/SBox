import 'package:flutter/material.dart';
import 'package:s_box/module/navigation_bottom_page.dart';
import 'package:s_box/module/splash/splash_page.dart';
import 'package:s_box/utils/locator.dart';

void main() {

  setupLocator();
  WidgetsFlutterBinding.ensureInitialized();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widgets is the root of your application.
  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyNavPage(),
    );
  }
}

